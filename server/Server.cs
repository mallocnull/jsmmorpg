﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SuperWebSocket;
using SuperSocket;
using SuperSocket.SocketBase;
using System.Configuration;

namespace server {
    class Server {
        private static WebSocketServer sock = new WebSocketServer();
        public static Dictionary<int, Player> onlinePlayers = new Dictionary<int, Player>();

        static void Main(string[] args) {
            Console.Write("Initializing database ... ");
            Database.Initialize();
            Console.WriteLine("OK");
            
            sock.NewMessageReceived += onMessageRecv;
            sock.NewSessionConnected += onNewConn;
            sock.SessionClosed += onCloseConn;
            
            Console.Write("Starting socket ... ");
            sock.Setup(1001);
            if(!sock.Start()) System.Console.WriteLine("ERR");
            else System.Console.WriteLine("OK");

            while(true) ;
        }

        public static int AddNewPlayer(Player p) {
            if(!onlinePlayers.ContainsKey(p.id)) {
                onlinePlayers.Add(p.id, p);
                return p.id;
            } else return -2;
        }

        private static void onNewConn(WebSocketSession session) {
        }

        private static void onCloseConn(WebSocketSession session, CloseReason reason) {
            foreach(Player p in onlinePlayers.Values) {
                if(p.sock.RemoteEndPoint == session.RemoteEndPoint) {
                    onlinePlayers.Remove(p.id);
                    break;
                }
            }
        }

        private static void onMessageRecv(WebSocketSession session, string value) {
            Console.WriteLine(value);

            int msgid = (int)value[0];
            value = value.Substring(1);
            string[] args = value.Split((char)255);

            switch(msgid) {
                case 1:
                    int pid;
                    if((pid = Clerical.Login(session, args[0], args[1])) > 0) {
                        Player p = onlinePlayers[pid];
                        session.Send(Message.Pack(1, "y", ""+pid, ""+p.map, ""+ p.position[0], ""+p.position[1]));
                    } else
                        session.Send(Message.Pack(1, "n", ((pid==-1)?"e":"l")));
                    break;
                case 2:
                    if(Clerical.Register(args[0], args[1]))
                        session.Send(Message.Pack(2, "y"));
                    else
                        session.Send(Message.Pack(2, "n"));
                    break;
                default:
                    // msgid 254 used for errors
                    session.Send(Message.Pack(254, "UNIMPLEMENTED_MSGID"));
                    break;
            }
        }
    }
}
