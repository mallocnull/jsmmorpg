﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace server {
    static class Message {
        public static string Pack(int msgid, params string[] parts) {
            return ((char)msgid) + String.Join(((char)255) + "", parts);
        }
    }
}
