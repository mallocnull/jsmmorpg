﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SuperWebSocket;
using SuperSocket;
using SuperSocket.SocketBase;
using System.Configuration;

namespace server {
    class Player {
        public WebSocketSession sock;
        public int id;
        public string username;
        public int[] position;
        public int map;

        public Player(WebSocketSession sock, int id, string username, int x, int y, int map) {
            this.id = id;
            this.sock = sock;
            this.username = username;
            this.position = new int[] { x, y };
            this.map = map;
        }
    }
}
