﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;

namespace server {
    static class Database {
        private static SQLiteConnection conn;

        public static void Initialize() {
            conn = new SQLiteConnection("Data Source=game.db;Version=3;New=False;Compress=True;");
            conn.Open();
        }

        static public T QueryScalar<T>(string query) {
            try {
                return (T)(new SQLiteCommand(query, conn)).ExecuteScalar();
            } catch(Exception e) {
                Console.WriteLine(e.Message);
                return default(T);
            }
        }

        static public bool QueryQuiet(string query) {
            try {
                (new SQLiteCommand(query, conn)).ExecuteNonQuery();
                return true;
            } catch(Exception e) {
                Console.WriteLine(e.Message);
                return false;
            }
        }

        static public List<object[]> QueryReader(string query) {
            try {
                List<object[]> data = new List<object[]>();
                var reader = (new SQLiteCommand(query, conn)).ExecuteReader();
                while(reader.Read()) {
                    data.Add(new object[reader.GetValues().Count]);
                    for(int i = 0; i < reader.GetValues().Count; i++)
                        data.Last()[i] = reader.GetValue(i);
                }
                return data;
            } catch(Exception e) {
                Console.WriteLine(e.Message);
                return null;
            }
        }
    }
}
