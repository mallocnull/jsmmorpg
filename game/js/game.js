﻿var GameContext = (function () {
    function GameContext(addr) {
        this.username = "";
        this.password = "";

        Socket.init(addr);
    }
    GameContext.prototype.isLoggedIn = function () {
        return this.username == "" && this.password == "";
    };

    GameContext.switchView = function (index) {
        var views = ["init", "login", "register", "game"];
        for (var i = 0; i < views.length; i++)
            document.getElementById(views[i]).style.display = (i == index) ? "block" : "none";
    };

    GameContext.prototype.attemptLogin = function () {
        var name = document.getElementById("loguser").value;
        var pwd = document.getElementById("logpwd").value;

        if (/^[0-9a-z]+$/i.test(name)) {
            document.getElementById("logbtn").style.display = "none";
            document.getElementById("reglink").style.display = "none";
            document.getElementById("logwait").style.display = "block";
            Socket.send(Message.pack(1, name, Message.hash(pwd)));
        } else
            alert("Username must not be empty and must contain only letters and numbers.");
    };

    GameContext.prototype.attemptRegistration = function () {
        var name = document.getElementById("reguser").value;
        var pwds = [
            document.getElementById("regpwd").value,
            document.getElementById("regpwdconf").value];

        if (pwds[0] == pwds[1]) {
            if (/^[0-9a-z]+$/i.test(name) && name.length <= 15) {
                document.getElementById("regbtn").style.display = "none";
                document.getElementById("regcbtn").style.display = "none";
                document.getElementById("regwait").style.display = "block";
                Socket.send(Message.pack(2, name, Message.hash(pwds[0])));
            } else
                alert("Username must not be empty and must contain only letters and numbers and be 15 characters or less.");
        } else
            alert("Passwords do not match!");
    };
    return GameContext;
})();

var game = new GameContext("ws://aroltd.com:1001");
