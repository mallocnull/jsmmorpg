﻿/// <reference path="cryptojs.d.ts" />
var Message = (function () {
    function Message() {
    }
    Message.pack = function (id) {
        var params = [];
        for (var _i = 0; _i < (arguments.length - 1); _i++) {
            params[_i] = arguments[_i + 1];
        }
        return String.fromCharCode(id) + params.join(this.seperator);
    };

    Message.hash = function (msg) {
        return CryptoJS.SHA3(msg, { outputLength: 512 }).toString();
    };
    Message.seperator = String.fromCharCode(255);
    return Message;
})();
