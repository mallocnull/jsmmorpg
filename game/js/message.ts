﻿/// <reference path="cryptojs.d.ts" />

class Message {
    static seperator = String.fromCharCode(255);

    static pack(id: number, ...params: string[]): string {
        return String.fromCharCode(id) + params.join(this.seperator);
    }

    static hash(msg: string): string {
        return CryptoJS.SHA3(msg, { outputLength: 512 }).toString();
    }
}