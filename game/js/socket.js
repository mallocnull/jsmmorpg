﻿var Socket = (function () {
    function Socket() {
    }
    Socket.init = function (addr) {
        this.sock = new WebSocket(addr);
        this.sock.onopen = this.onConnOpen;
        this.sock.onmessage = this.onMessageRecv;
        this.sock.onerror = this.onConnError;
        this.sock.onclose = this.onConnClose;
    };

    Socket.send = function (msg) {
        this.sock.send(msg);
    };

    Socket.onMessageRecv = function (e) {
        alert(e.data);

        var msgid = e.data.charCodeAt(0);
        var parts = e.data.substr(1).split(Message.seperator);

        switch (msgid) {
            case 1:
                if (parts[0] == "y") {
                    GameContext.switchView(3);
                    // TODO handle game loading etc etc
                } else {
                    if (parts[1] == "e")
                        alert("Username and/or password incorrect!");
                    else
                        alert("User is already logged in!");
                }

                document.getElementById("logbtn").style.display = "inline";
                document.getElementById("reglink").style.display = "inline";
                document.getElementById("logwait").style.display = "none";
                break;
            case 2:
                if (parts[0] == "y") {
                    GameContext.switchView(1);
                    alert("Registration successful!");
                } else
                    alert("Username is taken!");

                document.getElementById("regbtn").style.display = "inline";
                document.getElementById("regcbtn").style.display = "inline";
                document.getElementById("regwait").style.display = "none";
                break;
        }
    };

    Socket.onConnOpen = function (e) {
        GameContext.switchView(1);
    };

    Socket.onConnError = function (e) {
        document.body.innerHTML = "<p>Connection could not be established.</p>";
    };

    Socket.onConnClose = function (e) {
    };
    return Socket;
})();
