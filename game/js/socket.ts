﻿class Socket {
    static sock: WebSocket;

    static init(addr: string) {
        this.sock = new WebSocket(addr);
        this.sock.onopen = this.onConnOpen;
        this.sock.onmessage = this.onMessageRecv;
        this.sock.onerror = this.onConnError;
        this.sock.onclose = this.onConnClose;
    }

    static send(msg: string) {
        this.sock.send(msg);
    }

    static onMessageRecv(e) {
        alert(e.data);

        var msgid = (<string>e.data).charCodeAt(0);
        var parts = (<string>e.data).substr(1).split(Message.seperator);

        switch (msgid) {
            case 1:
                if (parts[0] == "y") {
                    GameContext.switchView(3);
                    // TODO handle game loading etc etc
                } else {
                    if (parts[1] == "e")
                        alert("Username and/or password incorrect!");
                    else
                        alert("User is already logged in!");
                }

                document.getElementById("logbtn").style.display = "inline";
                document.getElementById("reglink").style.display = "inline";
                document.getElementById("logwait").style.display = "none";
                break;
            case 2:
                if (parts[0] == "y") {
                    GameContext.switchView(1);
                    alert("Registration successful!");
                } else
                    alert("Username is taken!");

                document.getElementById("regbtn").style.display = "inline";
                document.getElementById("regcbtn").style.display = "inline";
                document.getElementById("regwait").style.display = "none";
                break;
        }
    }

    static onConnOpen(e) {
        GameContext.switchView(1);
    }

    static onConnError(e) {
        document.body.innerHTML = "<p>Connection could not be established.</p>";
    }

    static onConnClose(e) {

    }
}