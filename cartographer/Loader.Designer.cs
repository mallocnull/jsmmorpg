﻿namespace cartographer {
    partial class Loader {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if(disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.OpenDialog = new System.Windows.Forms.OpenFileDialog();
            this.UrlBox = new System.Windows.Forms.TextBox();
            this.OpenUrl = new System.Windows.Forms.Button();
            this.Cancel = new System.Windows.Forms.Button();
            this.LoadFolder = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // OpenDialog
            // 
            this.OpenDialog.FileName = "openFileDialog1";
            // 
            // UrlBox
            // 
            this.UrlBox.Location = new System.Drawing.Point(13, 13);
            this.UrlBox.Name = "UrlBox";
            this.UrlBox.Size = new System.Drawing.Size(320, 20);
            this.UrlBox.TabIndex = 0;
            // 
            // OpenUrl
            // 
            this.OpenUrl.Location = new System.Drawing.Point(13, 40);
            this.OpenUrl.Name = "OpenUrl";
            this.OpenUrl.Size = new System.Drawing.Size(75, 23);
            this.OpenUrl.TabIndex = 1;
            this.OpenUrl.Text = "Open URL";
            this.OpenUrl.UseVisualStyleBackColor = true;
            // 
            // Cancel
            // 
            this.Cancel.Location = new System.Drawing.Point(257, 40);
            this.Cancel.Name = "Cancel";
            this.Cancel.Size = new System.Drawing.Size(75, 23);
            this.Cancel.TabIndex = 2;
            this.Cancel.Text = "Cancel";
            this.Cancel.UseVisualStyleBackColor = true;
            // 
            // LoadFolder
            // 
            this.LoadFolder.Location = new System.Drawing.Point(176, 40);
            this.LoadFolder.Name = "LoadFolder";
            this.LoadFolder.Size = new System.Drawing.Size(75, 23);
            this.LoadFolder.TabIndex = 3;
            this.LoadFolder.Text = "Load Folder";
            this.LoadFolder.UseVisualStyleBackColor = true;
            // 
            // Loader
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(345, 73);
            this.ControlBox = false;
            this.Controls.Add(this.LoadFolder);
            this.Controls.Add(this.Cancel);
            this.Controls.Add(this.OpenUrl);
            this.Controls.Add(this.UrlBox);
            this.Name = "Loader";
            this.ShowInTaskbar = false;
            this.Text = "Loader";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.OpenFileDialog OpenDialog;
        private System.Windows.Forms.TextBox UrlBox;
        private System.Windows.Forms.Button OpenUrl;
        private System.Windows.Forms.Button Cancel;
        private System.Windows.Forms.Button LoadFolder;
    }
}