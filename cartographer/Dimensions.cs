﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace cartographer {
    public partial class Dimensions : Form {
        public bool success;
        public int width, height;

        public Dimensions() {
            InitializeComponent();
        }

        public void InitTextboxes(int w, int h) {
            mapWidth.Text = w.ToString();
            mapHeight.Text = h.ToString();
        }

        private void button1_Click(object sender, EventArgs e) {
            try {
                width = Int32.Parse(mapWidth.Text);
                height = Int32.Parse(mapHeight.Text);
                success = true;
            } catch {
                success = false;
            }
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e) {
            success = false;
            this.Close();
        }
    }
}
