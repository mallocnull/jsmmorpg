﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace cartographer {
    public partial class Form1 : Form {
        public Form1() {
            InitializeComponent();
        }

        private void newToolStripMenuItem_Click(object sender, EventArgs e) {
            Dimensions d = new Dimensions();
            d.ShowDialog();
            if(d.success) {
                Map.Init(d.width, d.height);
                RedrawMap();
            }
        }

        public void RedrawMap() {
            Splitter.Panel2.Invalidate();
        }

        private void Splitter_Panel2_Paint(object sender, PaintEventArgs e) {

        }
    }
}
