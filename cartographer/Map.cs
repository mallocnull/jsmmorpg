﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cartographer {
    static class Map {
        public static Tile[,] tiles;
        public static int x, y;

        public static void Init(int width, int height, int value = 1) {
            tiles = new Tile[width, height];
            for(int i = 0; i < height; i++) {
                for(int j = 0; j < width; j++)
                    tiles[j, i] = new Tile(value);
            }

            x = y = 0;
        }
    }
}
