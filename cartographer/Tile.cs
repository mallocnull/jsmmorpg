﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cartographer {
    class Tile {
        private int tileId;
        TileProperties properties;

        public Tile() {
            this.tileId = 1;
        }

        public Tile(int tileId) {
            this.tileId = tileId;
        }
    }

    class TileProperties {
        private bool canCollide;
        private byte zindex;

        public bool CanCollide {
            get { return canCollide; }
            set { canCollide = value; }
        }

        public byte zIndex {
            get { return zindex; }
            set { zindex = value; }
        }
    }
}
